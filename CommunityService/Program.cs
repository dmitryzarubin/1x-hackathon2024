using CommunityService.Db;
using CommunityService.HealthChecks;
using CommunityService.Model;
using CommunityService.Requests;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using MongoDB.Driver;

namespace CommunityService;

public class Program
{
    public static void Main(string[] args)
    {
        var builder = WebApplication.CreateBuilder(args);

        builder.Services.AddLogging(loggingBuilder =>
        {
            loggingBuilder.AddConsole();
        });

        builder.Services.AddServiceHealthChecks(builder.Configuration);

        builder.Services.AddEndpointsApiExplorer();
        builder.Services.AddSwaggerGen();

        var databaseName = builder.Configuration["primaryDb:databaseName"];
        var databaseConnection = builder.Configuration["primaryDb:connectionString"];
        var mongoUrlBuilder = new MongoUrlBuilder(databaseConnection)
        {
            
            DatabaseName = databaseName
        };
        
        builder.Services.Configure<ContextOptions>(options => options.Database = databaseName);
        builder.Services.AddSingleton<Context>(sp => new Context(new MongoClient(mongoUrlBuilder.ToMongoUrl()), sp.GetRequiredService<IOptions<ContextOptions>>()));
        builder.Services.AddHostedService<ScoringHostedService>();
        
        
        var app = builder.Build();

        app.UseSwagger();
        app.UseSwaggerUI();
        

        app.UseExceptionHandler(exceptionHandlerApp =>
            exceptionHandlerApp.Run(async context =>
            {
                await Results.Problem().ExecuteAsync(context);
            }));


        app
            .MapGet("api/community", (Context context) =>
            {
                var communities = context.Communities.Find(Builders<Community>.Filter.Empty).ToList();
                return Results.Ok(communities);
            })
            .WithName("GetCommunityList")
            .WithOpenApi()
            .Produces<IEnumerable<Community>>();

        app
            .MapGet("api/community/{id}", ([FromQuery] Guid id, Context context) =>
            {
                var community = context.Communities.Find(Builders<Community>.Filter.Eq(x=>x.CommunityId, id)).SingleOrDefault();
                return Results.Ok(community);
            })
            .WithName("GetCommunityById")
            .WithOpenApi()
            .Produces<Community>();

        app
            .MapPost("api/community", async ([FromBody] CreateCommunityRequest request, Context context) =>
            {
                var community = new Community
                {
                    CommunityId = request.CommunityId,
                    CategoryId = request.CategoryId,
                    Title = request.Title,
                    Description = request.Description,
                    Tags = request.Tags,
                    CreationDate = DateTime.UtcNow,
                    Status = CommunityStatus.New,
                    ReddyLink = request.ReddyLink,
                    CreatorId = request.CreatorId
                };
                
                context.Communities.InsertOne(community);
                return Results.NoContent();
            })
            .WithName("CreateCommunity")
            .WithOpenApi()
            .Produces(StatusCodes.Status204NoContent)
            .ProducesProblem(StatusCodes.Status400BadRequest);
        
        app
            .MapPost("api/community/{id}/join", async ([FromQuery] Guid id, [FromBody] JoinToCommunityRequest request, Context context) =>
            {
                var community = await context.Communities.Find(x => x.CommunityId == id).SingleOrDefaultAsync();
                if (community == null)
                    return Results.NotFound();

                await context.Communities.UpdateOneAsync(x => x.CommunityId == id,
                    Builders<Community>.Update
                        .AddToSet(x => x.Participants, request.UserId)
                        .Set(x => x.CountOfParticipants, @community.Participants.Count())
                    );

                return Results.NoContent();
            })
            .WithName("JoinToCommunity")
            .WithOpenApi()
            .Produces(StatusCodes.Status204NoContent)
            .ProducesProblem(StatusCodes.Status400BadRequest)
            .ProducesProblem(StatusCodes.Status404NotFound);


        
        
        
        
        
        
        
        
        app
            .MapGet("api/event", (Context context) =>
            {
                var events = context.Events.Find(Builders<Event>.Filter.Empty).ToList();
                return Results.Ok(events);
            })
            .WithName("GetEventList")
            .WithOpenApi()
            .Produces<IEnumerable<Event>>();

        app
            .MapGet("api/event/{id}", ([FromQuery] Guid id, Context context) =>
            {
                var @event = context.Events.Find(Builders<Event>.Filter.Eq(x=>x.EventId, id)).SingleOrDefault();
                return Results.Ok(@event);
            })
            .WithName("GetEventById")
            .WithOpenApi()
            .Produces<Event>();

        app
            .MapPost("api/event", async ([FromBody] CreateCommunityEventRequest request, Context context) =>
            {
                var @event = new Event()
                {
                    EventId = request.EventId,
                    CommunityId = request.CommunityId,
                    CategoryId = request.CategoryId,
                    Title = request.Title,
                    Description = request.Description,
                    Tags = request.Tags,
                    ReddyLink = request.ReddyLink,
                    ExternalLink = request.ExternalLink,
                    CreatorId = request.CreatorId,
                    StartDate = request.StartDate,
                    EndDate = request.EndDate,
                    Location = request.Location
                };
                
                await context.Events.InsertOneAsync(@event);
                return Results.NoContent();
            })
            .WithName("CreateCommunityEvent")
            .WithOpenApi()
            .Produces(StatusCodes.Status204NoContent)
            .ProducesProblem(StatusCodes.Status400BadRequest);


        app
            .MapPost("api/event/{id}/join", async ([FromQuery] Guid id, [FromBody] JoinToEventRequest request, Context context) =>
            {
                var @event = await context.Events.Find(x => x.EventId == id).SingleOrDefaultAsync();
                if (@event == null)
                    return Results.NotFound();
                
                await context.Events.UpdateOneAsync(x => x.EventId == id,
                    Builders<Event>.Update
                        .AddToSet(x => x.Participants, request.UserId)
                        .Set(x => x.CountOfParticipants, @event.Participants.Count() + 1)
                );

                return Results.NoContent();
            })
            .WithName("JoinToEvent")
            .WithOpenApi()
            .Produces(StatusCodes.Status204NoContent)
            .ProducesProblem(StatusCodes.Status400BadRequest)
            .ProducesProblem(StatusCodes.Status404NotFound);

        app
            .MapPost("api/event/{id}/like", async ([FromQuery] Guid id, [FromBody] LikeEventRequest request, Context context) =>
            {
                var @event = await context.Events.Find(x => x.EventId == id).SingleOrDefaultAsync();
                if (@event == null)
                    return Results.NotFound();
                
                var like = await (await context.Likes.FindAsync(x => x.UserId == request.UserId && x.EventId == @event.EventId)).SingleOrDefaultAsync();
                if (like != null)
                    return Results.Conflict("Already liked");

                await context.Likes.InsertOneAsync(new Like
                {
                    EventId = @event.EventId,
                    UserId = request.UserId
                });

                var statistics = @event.Likes.Statistics;
                var emotionsItem = statistics.Single(x => x.EmotionType == request.EmotionType);
                emotionsItem.Count++;

                await context.Events.UpdateOneAsync(x => x.EventId == id,
                    Builders<Event>.Update.Set(x => x.Likes, @event.Likes)
                );

                return Results.NoContent();
            })
            .WithName("LikeEvent")
            .WithOpenApi()
            .Produces(StatusCodes.Status204NoContent)
            .ProducesProblem(StatusCodes.Status409Conflict);
        
        app
            .MapPost("api/authorize", async ([FromBody] AuthRequest request, [FromServices] Context context) =>
            {
                Guid userId = Guid.Empty;
                if (request.Login == "1")
                {
                    userId  = Guid.Parse("");
                }
                
                var user = await context.Users.Find(x => x.UserId == userId ).SingleOrDefaultAsync();
               if (user == null)
                    return Results.Unauthorized();

               return Results.Ok(user);
            })
            .WithName("Auth")
            .WithOpenApi()
            .Produces<User>()
            .ProducesProblem(StatusCodes.Status401Unauthorized);
        
        
        app
            .MapGet("api/users", (Context context) =>
            {
                var users = context.Users.Find(Builders<User>.Filter.Empty).ToList();
                return Results.Ok(users);
            })
            .WithName("UsersList")
            .WithOpenApi()
            .Produces<IEnumerable<User>>();

        app.Run();
    }
}