using MongoDB.Bson.Serialization.Attributes;

#pragma warning disable CS8618

namespace CommunityService.Model;

public class Location
{
    [BsonElement] public string Title { get; set; }

    [BsonElement] public string Description { get; set; }

    [BsonElement] public string GoogleMapLink { get; set; }
}