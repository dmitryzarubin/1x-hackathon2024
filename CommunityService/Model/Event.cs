using System.Text.Json.Serialization;
using MongoDB.Bson.Serialization.Attributes;

namespace CommunityService.Model;

#pragma warning disable CS8618
public class Event
{
    [BsonId] public Guid EventId { get; set; }

    [BsonElement] public Guid? CommunityId { get; set; }

    [BsonElement] public DateTime StartDate { get; set; }

    [BsonElement] public DateTime EndDate { get; set; }

    [BsonElement] public string Title { get; set; }

    [BsonElement] public string Description { get; set; }

    [BsonElement] public Guid CategoryId { get; set; } = Guid.Empty;

    [BsonElement] public IEnumerable<string> Tags { get; set; }

    [BsonElement] public Location Location { get; set; }

    [BsonElement] public Guid CreatorId { get; set; }
    // public IEnumerable<Guid> Team { get; set; }

    [BsonElement] public int Value { get; set; }

    [BsonElement] public string ExternalLink { get; set; }

    [BsonElement] public string ReddyLink { get; set; }

    [BsonElement] public EventStatus Status { get; set; }

    [BsonElement] public int TokensForParticipants { get; set; }
    
    [BsonElement] public int CountOfParticipants { get; set; }

    [BsonElement] public EmotionsStatistics Likes { get; set; } = new();
    
    [BsonElement] [JsonIgnore] public IEnumerable<Guid> Participants { get; set; }

}