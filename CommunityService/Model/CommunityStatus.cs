namespace CommunityService.Model;

#pragma warning disable CS8618
public enum CommunityStatus
{
    New,
    OnModeration,
    Approved,
    Banned
}