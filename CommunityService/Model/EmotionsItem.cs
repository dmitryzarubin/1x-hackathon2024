using MongoDB.Bson.Serialization.Attributes;

namespace CommunityService.Model;

#pragma warning disable CS8618
public class EmotionsItem
{
    [BsonElement] public EmotionType EmotionType { get; set; }

    [BsonElement] public int Count { get; set; }
}