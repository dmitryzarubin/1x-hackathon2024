using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace CommunityService.Model;

public class Like
{
    [BsonId] ObjectId Id { get; set; }
    [BsonElement] public Guid EventId { get; set; }
    [BsonElement] public Guid UserId { get; set; }
}