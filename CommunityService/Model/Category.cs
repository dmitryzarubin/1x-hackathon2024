using MongoDB.Bson.Serialization.Attributes;

namespace CommunityService.Model;

public class Category
{
    [BsonId] public Guid CategoryId { get; set; }

    [BsonElement] public string Title { get; set; }
}