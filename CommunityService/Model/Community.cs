using System.Text.Json.Serialization;
using MongoDB.Bson.Serialization.Attributes;

namespace CommunityService.Model;

#pragma warning disable CS8618
public class Community
{
    [BsonId] public Guid CommunityId { get; set; }

    [BsonElement] public DateTime CreationDate { get; set; }

    [BsonElement] public string Title { get; set; }

    [BsonElement] public string Description { get; set; }
    [BsonElement] public string ImageUrl { get; set; }

    [BsonElement] public CommunityStatus Status { get; set; }

    [BsonElement] public Guid CategoryId { get; set; }

    [BsonElement] public IEnumerable<string> Tags { get; set; }

    [BsonElement] public string ReddyLink { get; set; }

    [BsonElement] public Guid CreatorId { get; set; }

    [BsonElement] public IEnumerable<Event> Events { get; set; }

    [BsonElement] public int CountOfParticipants { get; set; }
    
    [BsonElement] [JsonIgnore] public IEnumerable<Guid> Participants { get; set; }
}