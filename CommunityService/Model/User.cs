using MongoDB.Bson.Serialization.Attributes;

namespace CommunityService.Model;

#pragma warning disable CS8618
public class User
{
    [BsonId] public Guid UserId { get; set; }

    [BsonElement] public string Name { get; set; }

    [BsonElement] public UserRole Role { get; set; }
}