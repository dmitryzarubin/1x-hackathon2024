using MongoDB.Bson.Serialization.Attributes;

namespace CommunityService.Model;

#pragma warning disable CS8618
public class EmotionsStatistics
{
    [BsonElement] public List<EmotionsItem> Statistics { get; set; } = new();
}