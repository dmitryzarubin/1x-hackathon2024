namespace CommunityService.Model;

#pragma warning disable CS8618
public enum EventStatus
{
    New,
    Planned,
    Active,
    Finished
}