namespace CommunityService.Model;

public enum EmotionType
{
    Negative = 1,
    Sad = 2,
    Neutral = 3,
    Good = 4,
    Positive = 5
}