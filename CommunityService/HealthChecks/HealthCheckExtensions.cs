﻿using Microsoft.AspNetCore.Diagnostics.HealthChecks;
using Microsoft.Extensions.Diagnostics.HealthChecks;

namespace CommunityService.HealthChecks;

internal static class HealthCheckExtensions
{
    public static IServiceCollection AddServiceHealthChecks(this IServiceCollection services, IConfiguration configuration)
    {
        var builder = services.AddHealthChecks();

        builder.AddCheck("self", () => HealthCheckResult.Healthy("self like a ping"));
        //builder.AddMongoHealthCheck("mongodb", new MongoUrl(configuration["primaryDb:connectionString"]), HealthStatus.Unhealthy);

        return services;
    }

    public static IApplicationBuilder UseHealthChecks(this IApplicationBuilder app)
    {
        app.UseHealthChecks("/hc", new HealthCheckOptions
        {
            Predicate = _ => true
        });
        return app;
    }
}