using CommunityService.Db;
using CommunityService.Model;
using MongoDB.Driver;

namespace CommunityService;

public class ScoringHostedService : IHostedService
{
    private readonly Context _context;
    private readonly CancellationTokenSource _instanceCancellationToken;

    public ScoringHostedService(Context context)
    {
        _context = context;
        _instanceCancellationToken = new CancellationTokenSource();
    }

    public async Task StartAsync(CancellationToken cancellationToken)
    {
        new Thread(async () =>
        {
            while (!_instanceCancellationToken.IsCancellationRequested) await PerformScoringIteration();
        })
        {
            IsBackground = true
        }.Start();
    }

    public async Task StopAsync(CancellationToken cancellationToken)
    {
        _instanceCancellationToken.Cancel();
    }

    private async Task PerformScoringIteration()
    {
        var communities = await (await _context.Communities.FindAsync(FilterDefinition<Community>.Empty)).ToListAsync();

        var cursor = await _context.Events.FindAsync(
            Builders<Event>.Filter.Or(
                Builders<Event>.Filter.Eq(x => x.Status, EventStatus.New),
                Builders<Event>.Filter.Eq(x => x.Status, EventStatus.Planned)));

        var notScoredEvents = await cursor.ToListAsync();

        foreach (var notScoredEvent in notScoredEvents)
        {
            var community = communities.Single(x => x.CommunityId == notScoredEvent.CommunityId);

            var score = EventsScoringNeuralNet.Calc(notScoredEvent, community);

            await _context.Events.UpdateOneAsync(x => x.EventId == notScoredEvent.EventId,
                Builders<Event>.Update.Set(x => x.TokensForParticipants, score));
        }
    }
}