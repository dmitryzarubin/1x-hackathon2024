using System.ComponentModel.DataAnnotations;
using CommunityService.Model;

namespace CommunityService.Requests;

public class CreateCommunityEventRequest
{
    [Required] public Guid EventId { get; set; }
    [Required] public Guid CommunityId { get; set; }
    [Required] public DateTime StartDate { get; set; }
    [Required] public DateTime EndDate { get; set; }
    [Required] public string Title { get; set; }
    [Required] public string Description { get; set; }
    [Required] public Guid CategoryId { get; set; }
    [Required] public IEnumerable<string> Tags { get; set; }
    [Required] public Location Location { get; set; }
    [Required] public Guid CreatorId { get; set; }
    [Required] public string ExternalLink { get; set; }
    [Required] public string ReddyLink { get; set; }
}