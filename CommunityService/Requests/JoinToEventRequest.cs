using System.ComponentModel.DataAnnotations;

namespace CommunityService.Requests;

public class JoinToEventRequest
{
    [Required] public Guid EventId { get; set; }
    [Required] public Guid UserId { get; set; }
}