using System.ComponentModel.DataAnnotations;

namespace CommunityService.Requests;

public class JoinToCommunityRequest
{
    [Required] public Guid CommunityId { get; set; }
    [Required] public Guid UserId { get; set; }
}