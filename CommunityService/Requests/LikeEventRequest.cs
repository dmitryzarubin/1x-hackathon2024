using System.ComponentModel.DataAnnotations;
using CommunityService.Model;

namespace CommunityService.Requests;

public class LikeEventRequest
{
    [Required] public Guid UserId { get; set; }
    [Required] public EmotionType EmotionType { get; set; }
}