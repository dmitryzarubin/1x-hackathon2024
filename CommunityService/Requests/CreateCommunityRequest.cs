using System.ComponentModel.DataAnnotations;

namespace CommunityService.Requests;

public class CreateCommunityRequest
{
    [Required] public Guid CommunityId { get; set; }

    [Required] public string Title { get; set; }

    [Required] public string Description { get; set; }
    
    [Required] public string ImageUrl { get; set; }

    [Required] public Guid CategoryId { get; set; }

    [Required] public IEnumerable<string> Tags { get; set; }

    [Required] public string ReddyLink { get; set; }

    [Required] public Guid CreatorId { get; set; }
}