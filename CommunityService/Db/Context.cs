using CommunityService.Model;
using Microsoft.Extensions.Options;
using MongoDB.Driver;

namespace CommunityService.Db;

public class Context
{
    public Context(IMongoClient mongoClient, IOptions<ContextOptions> options)
    {
        Communities = mongoClient.GetDatabase(options.Value.Database).GetCollection<Community>("Communities");
        Events = mongoClient.GetDatabase(options.Value.Database).GetCollection<Event>("Events");
        Users = mongoClient.GetDatabase(options.Value.Database).GetCollection<User>("Users");
        Likes = mongoClient.GetDatabase(options.Value.Database).GetCollection<Like>("Likes");
    }

    public IMongoCollection<Community> Communities { get; }
    public IMongoCollection<Event> Events { get; }
    public IMongoCollection<User> Users { get; }
    public IMongoCollection<Like> Likes { get; }
}