using CommunityEventML;
using CommunityService.Model;

namespace CommunityService;

public static class EventsScoringNeuralNet
{
    public static int Calc(Event @event, Community community)
    {
        MLModel1.ModelInput sampleData = new MLModel1.ModelInput()
        {
            CommunityMemberCnt = 1F,
            Value = 0F,
            MemberCnt = 1F,
            E1 = 2F,
            E2 = 1F,
            E3 = 2F,
            E4 = 1F,
        };


        var modelOutput = MLModel1.Predict(sampleData);
        var score = (int)Math.Round(modelOutput.Score);
        return score;
    }
}