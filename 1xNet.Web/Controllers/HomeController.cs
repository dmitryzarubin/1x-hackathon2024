using System.Diagnostics;
using _1xNet.Web.Data;
using Microsoft.AspNetCore.Mvc;
using _1xNet.Web.Models;

namespace _1xNet.Web.Controllers;

public class HomeController(ILogger<HomeController> logger, FakeData fakeData) : Controller
{
    private readonly ILogger<HomeController> _logger = logger;

    public IActionResult Index()
    {
        var viewModel = fakeData.GetIndexViewModel();
        return View(viewModel);
    }

    public IActionResult Privacy()
    {
        return View();
    }

    [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
    public IActionResult Error()
    {
        return View(new ErrorViewModel {RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier});
    }
}