using System.Diagnostics;
using _1xNet.Web.Data;
using _1xNet.Web.Models;
using _1xNet.Web.Models.Community;
using _1xNet.Web.Models.Event;
using CommunityService.OpenApi;
using Microsoft.AspNetCore.Mvc;

namespace _1xNet.Web.Controllers;

public class EventController(CommunityServiceClient communityServiceClient)
{
    //CommunityServiceClient _communityServiceClient = communityServiceClient;

    public async Task<IActionResult> Add(NewEventViewModel model)
    {
        // await _communityServiceClient.CreateCommunityEventAsync(new CreateCommunityEventRequest()
        // {
        //     CommunityId = model.Community.Id,
        //     Description = model.Description,
        //     ReddyLink = model.ReddyLink,
        //     Location = model.ReddyLink,
        //     EventId = Guid.NewGuid(),
        //     CategoryId = Guid.NewGuid(),
        //     CreatorId = Guid.NewGuid(),
        //     Tags = new List<string>(){ model.}
        // });
        return null;
    }
}

public class CommunityController(
    FakeData fakeData) : Controller
{
    [HttpGet]
    public IActionResult List(string tag = "")
    {
        var viewModel = fakeData.GetCommunityViewModel();
        var tagViewModels = viewModel.Items.SelectMany(x => x.Tags).Distinct().ToArray();
        viewModel.Tags = tagViewModels;

        if (!string.IsNullOrEmpty(tag))
            viewModel.Items = viewModel.Items.Where(x => x.Tags.Any(t => t.Name == tag)).ToArray();

        return View(viewModel);
    }

    public IActionResult Index(Guid id, bool isNew = false)
    {
        var viewModel = fakeData.GetCommunity(id);
        viewModel.IsJoinedNow = isNew;
        return View(viewModel);
    }

    public IActionResult Join(Guid id)
    {
        return RedirectToAction("Index", new {id = id, isNew = true});
    }

    [HttpPost]
    public async Task<IActionResult> Add(NewCommunityItemViewModel model)
    {
        var request = new CreateCommunityRequest()
        {
            Title = model.Title,
            Description = model.Description,
            ReddyLink = model.ReddyLink,
            ImageUrl = model.ImageUrl,
            Tags = new List<string>() {model.Tag},
            CommunityId = Guid.NewGuid(),
            CreatorId = Guid.NewGuid(),
            CategoryId = Guid.NewGuid()
        };

        //await communityServiceClient.CreateCommunityAsync(request);
        //return RedirectToAction("Index", new {id = request.CommunityId});

        return RedirectToAction("List");
    }

    [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
    public IActionResult Error()
    {
        return View(new ErrorViewModel {RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier});
    }
}