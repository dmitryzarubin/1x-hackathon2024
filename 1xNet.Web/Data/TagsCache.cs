using System.Collections.Concurrent;

namespace _1xNet.Web.Data;

public static class TagsCache
{
    private static int _counter = 1;
    private static ConcurrentDictionary<string, int> _ints = new();
    public static int GetId(string tag)
    {
        var tagId = _ints.GetOrAdd(tag, s =>
        {
            _counter++;
            return _counter;
        });

        return tagId;
    }
}