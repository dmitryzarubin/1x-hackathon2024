using _1xNet.Web.Models;
using _1xNet.Web.Models.Community;
using _1xNet.Web.Models.Event;
using _1xNet.Web.Models.Home;
using CommunityService.OpenApi;

namespace _1xNet.Web.Data;

public class FakeData
{
    public CommunityListViewModel GetCommunityViewModel()
    {
        // var communityServiceClient = new CommunityServiceClient(new HttpClient
        // {
        //     BaseAddress = new Uri("http://localhost:5001")
        // });
        // var communities = communityServiceClient.GetCommunityListAsync().GetAwaiter().GetResult();
        //
        // var items = new CommunityListViewModel
        // {
        //     Items = communities.Select(c => Map(c)).ToArray()
        // };
        //
        // return items;
        
        var items = new CommunityListViewModel
        {
            Items = new List<CommunityItemViewModel>
            {
                GetCommunity1(),
                GetCommunity2()
            }
        };
        return items;
    }

    public CommunityItemViewModel Map(Community community)
    {
        return new CommunityItemViewModel
        {
            Id = community.CommunityId,
            Title = community.Title,
            Description = community.Description,
            ReddyLink = community.ReddyLink,
            ImageUrl = community.ImageUrl,
            Tags = community.Tags.Select(t => new TagViewModel
            {
                Id = TagsCache.GetId(t),
                Name = t
            }).ToArray(),
            MembersCount = community.CountOfParticipants,
        };
    }
    
    
    public EventsViewModel GetEventsViewModel()
    {
        var items = new EventsViewModel
        {
            Items = new List<EventViewModel>
            {
                GetEvent1(),
                GetEvent2()
            }
        };
        return items;
    }

    public CommunityItemViewModel GetCommunity1()
    {
        return new CommunityItemViewModel
        {
            Id = new Guid("00000000-0000-0000-0000-000000000001"),
            Title = "1xMarket|Барахолка",
            Description = "продаем/отдаем новым и б/у вещи",
            ReddyLink = "https://lnk.reddy.team/invite/_p0iA_Z2DGWqRRWpW_hIp4Zt1JKrzF7R",
            ImageUrl = "https://tse3.mm.bing.net/th/id/OIG3.JgerWYgk8xEvb.oCEAGJ?pid=ImgGn",
            Tags = new List<TagViewModel>
            {
                new TagViewModel {Name = "Барахолка", Id = 1}
            }.ToArray(),
            MembersCount = 983,
            Category = GetCategoryBuy()
        };
    }
    public CommunityItemViewModel GetCommunity2()
    {
        return new CommunityItemViewModel
        {
            Id = new Guid("00000000-0000-0000-0000-000000000002"),
            Title = "Футбол",
            Description = "группа по футболу",
            ReddyLink = "https://lnk.reddy.team/invite/PZZqWiwHVob_ub4pZ8P3y1lOGAu2ih3X",
            ImageUrl = "https://tse3.mm.bing.net/th/id/OIG1.HnToGC36FpoXELdx.vPV?pid=ImgGn",
            Tags = new List<TagViewModel>
            {
                new TagViewModel {Name = "Футбол", Id = 2}
            }.ToArray(),
            MembersCount = 155,
            Events = new EventsViewModel(){ Items = new List<EventViewModel>(){ GetEvent1(), GetEvent2() } },
            Category = GetCategorySport()
                
        };
    }

    public CommunityItemViewModel? GetCommunity(Guid id)
    {
        return GetCommunityViewModel().Items.FirstOrDefault(x => x.Id == id);
    }
    
    public EventViewModel GetEvent1()
    {
        return new EventViewModel
        {
            EventId = Guid.NewGuid(),
            Community = new IdNameViewModel {Id = GetCommunity1().Id, Name = GetCommunity1().Title},
            StartDate = DateTime.Now,
            EndDate = DateTime.Now.AddHours(2),
            Title = "Игра в футбол",
            Description = "Играем в футбол",
            Category = GetCategorySport(),
            Tags = new List<string> {"Футбол"},
            Location = new LocationViewModel(){ Description = "Omonoias Avenue &, Thalia, Limassol 3045, Кипр", GoogleMapLink = "https://maps.app.goo.gl/3VaDj75SWdG98z4c7", Title = "Kick on Fives"},
            //Location = new LocationViewModel {Latitude = 55.7558, Longitude = 37.6176},
            Creator = GetCreator1(),
            Value = 0,
            ExternalLink = "https://lnk.reddy.team/invite/PZZqWiwHVob_ub4pZ8P3y1lOGAu2ih3X",
            ReddyLink = "https://lnk.reddy.team/invite/PZZqWiwHVob_ub4pZ8P3y1lOGAu2ih3X",
            TokensForParticipants = 0,
            TokensForCreator = 0,
            PlannedCountOfParticipants = 0,
            ActualCountOfParticipants = 0
        };
    }
    
    public EventViewModel GetEvent2()
    {
        return new EventViewModel
        {
            EventId = Guid.NewGuid(),
            Community = new IdNameViewModel {Id = GetCommunity1().Id, Name = GetCommunity1().Title},
            StartDate = new DateTime(2023,11,3),
            ImageUrl = "https://tse2.mm.bing.net/th/id/OIG3.9KHuNIicKKFVOVwwZJqS?pid=ImgGn",
            EndDate = new DateTime(2023,12,3),
            Title = "Корпоративный чемпионат по мини-футболу",
            Description = "Соревнование пройдет между отделами нашей компании.",
            Category = GetCategorySport(),
            Tags = new List<string> {"Футбол"},
            Location = new LocationViewModel(){ Description = "Omonoias Avenue &, Thalia, Limassol 3045, Кипр", GoogleMapLink = "https://maps.app.goo.gl/3VaDj75SWdG98z4c7", Title = "Kick on Fives"},
            Creator = GetCreator2(),
            Value = 5,
            ExternalLink = "https://lnk.reddy.team/invite/PZZqWiwHVob_ub4pZ8P3y1lOGAu2ih3X",
            ReddyLink = "https://web.reddy.team/#/handle/chat/787117/message/668",
            TokensForParticipants = 5,
            TokensForCreator = 5,
            PlannedCountOfParticipants = 150,
            ActualCountOfParticipants = 170
        };
    }

    public IdNameViewModel GetCategorySport()
    {
        return new IdNameViewModel(){ Id = Guid.NewGuid(), Name = "Спорт"};
    }
    public IdNameViewModel GetCategory2()
    {
        return new IdNameViewModel(){ Id = Guid.NewGuid(), Name = "Футбол"};
    }
    public IdNameViewModel GetCategoryBuy()
    {
        return new IdNameViewModel(){ Id = Guid.NewGuid(), Name = "Барахолка"};
    }
    
    public IdNameViewModel GetCreator1()
    {
        return new IdNameViewModel(){ Id = Guid.NewGuid(), Name = "Иванов Иван"};
    }
    public IdNameViewModel GetCreator2()
    {
        return new IdNameViewModel(){ Id = Guid.NewGuid(), Name = "Петров Петр"};
    }

    public IndexViewModel GetIndexViewModel()
    {
        return new IndexViewModel()
        {
            News = [GetNews1(),GetNews2(),GetNews3()],
            PopularEvents = [GetEvent1()],
            PopularCommunities = [GetCommunity2(), GetCommunity1()],
            User = GetUserViewModel()
        };
    } 
    
    public NewsItemViewModel GetNews1()
    {
        return new NewsItemViewModel
        {
            Id = new Guid("00000000-0000-0000-0000-000000000002"),
            Title = "Летняя стажировка для детей X-сотрудников!",
            Description = "Актуальный вопрос для родителей школьников и студентов - как провести лето, чтобы совместить отдых детей с пользой?",
            ImageUrl = "/static/news1.png",
            CreatedAt = DateTime.Now.AddHours(-35),
            ReddyLink = "https://web.reddy.team/#/handle/chat/62716/message/35094"
        };
    }
    
    public NewsItemViewModel GetNews2()
    {
        return new NewsItemViewModel
        {
            Id = new Guid("00000000-0000-0000-0000-000000000003"),
            Title = "Корпоративный хакатон \"EASY HACK\"",
            Description = "Чтобы внести больше ясности, сообщаем, что перед участниками не будет стоять конкретной проблемы, которую нужно решить.",
            ImageUrl = "/static/news2.jpeg",
            CreatedAt = DateTime.Now.AddHours(-125),
            ReddyLink = "https://web.reddy.team/#/handle/chat/62716/message/34870"
        };
    }
    public NewsItemViewModel GetNews3()
    {
        return new NewsItemViewModel
        {
            Id = new Guid("00000000-0000-0000-0000-000000000004"),
            Title = "Партнерская программа",
            Description = "Коллеги, добрый день!\n\nРады сообщить о запуске нового проекта «Партнерская программа» на xEvents.",
            ImageUrl = "https://tse4.mm.bing.net/th/id/OIG2._emWMdPbgXSZo_30ZXd.?pid=ImgGn",
            CreatedAt = DateTime.Now.AddHours(-155),
            ReddyLink = "https://web.reddy.team/#/handle/chat/62716/message/34859"
        };
    }

    public UserViewModel GetUserViewModel()
    {
        return new UserViewModel()
        {
            Name = "Молотков Сергей",
            Rating = new decimal(4.4),
            TokenCount = 31
        };
    }
}