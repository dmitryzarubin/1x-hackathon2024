namespace _1xNet.Web.Models.Home;

public class NewsItemViewModel
{
    public Guid Id { get; set; }
    public string Title { get; set; }
    public string Description { get; set; }
    public string ImageUrl { get; set; }
    public DateTime CreatedAt { get; set; }
    public int CommentsCount { get; set; }
    public int LikesCount { get; set; }
    public int ViewsCount { get; set; }
    public string ReddyLink { get; set; }
}