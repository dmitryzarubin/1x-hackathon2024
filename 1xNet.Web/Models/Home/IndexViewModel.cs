using _1xNet.Web.Models.Community;
using _1xNet.Web.Models.Event;

namespace _1xNet.Web.Models.Home;

public class IndexViewModel
{
    public NewsItemViewModel[] News { get; set; }
    public EventViewModel[] PopularEvents { get; set; }
    public CommunityItemViewModel[] PopularCommunities { get; set; }
    public UserViewModel User { get; set; }
}