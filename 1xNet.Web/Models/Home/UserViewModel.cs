namespace _1xNet.Web.Models.Home;

public class UserViewModel
{
    public string Name { get; set; }
    public int TokenCount { get; set; }
    public decimal Rating { get; set; }
}