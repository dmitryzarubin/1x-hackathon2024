namespace _1xNet.Web.Models.Event;

public class LocationViewModel
{
    public string Title { get; set; }
    public string Description { get; set; }
    public string GoogleMapLink { get; set; }
}