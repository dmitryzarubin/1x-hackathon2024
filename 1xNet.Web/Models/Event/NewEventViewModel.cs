namespace _1xNet.Web.Models.Event;

public class NewEventViewModel
{
    public Guid EventId { get; set; }
    public Guid? Community { get; set; }
    public DateTime EndDate { get; set; }
    public string Title { get; set; }
    public string Description { get; set; }
    public string Category { get; set; }
    public string Tag { get; set; }
    public string LocationLink { get; set; }
    public string ReddyLink { get; set; }
}