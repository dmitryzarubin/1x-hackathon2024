namespace _1xNet.Web.Models.Event;

public class EventViewModel
{
    public Guid EventId { get; set; }
    public IdNameViewModel? Community { get; set; }
    public DateTime StartDate { get; set; }
    public string ImageUrl { get; set; }
    public DateTime EndDate { get; set; }
    public string Title { get; set; }
    public string Description { get; set; }
    public IdNameViewModel Category { get; set; }
    public IEnumerable<string> Tags { get; set; }
    public LocationViewModel Location { get; set; }
    public IdNameViewModel Creator { get; set; }
    public int Value { get; set; }
    public string ExternalLink { get; set; }
    public string ReddyLink { get; set; }
    //public EventStatus Status { get; set; }
    public int TokensForParticipants { get; set; }
    public int TokensForCreator { get; set; }
    public int PlannedCountOfParticipants { get; set; }
    //public EmotionsStatisticsViewModel BeforeEvent { get; set; }
    public int ActualCountOfParticipants { get; set; }
    //public EmotionsStatisticsViewModel Feedback { get; set; }
}