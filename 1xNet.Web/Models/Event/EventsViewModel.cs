namespace _1xNet.Web.Models.Event;

public class EventsViewModel
{
    public IList<EventViewModel> Items { get; set; }
}