namespace _1xNet.Web.Models;

public class IdNameViewModel
{
    public Guid Id { get; set; }
    public string Name { get; set; }
}