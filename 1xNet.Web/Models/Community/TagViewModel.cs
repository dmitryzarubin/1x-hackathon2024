namespace _1xNet.Web.Models.Community;

public class TagViewModel
{
    public string Name { get; set; }
    public int Id { get; set; }
}