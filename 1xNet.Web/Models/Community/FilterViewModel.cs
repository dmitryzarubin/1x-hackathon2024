namespace _1xNet.Web.Models.Community;

public class FilterViewModel
{
    public string SearchText { get; set; }
    public int[] TagId { get; set; }
}