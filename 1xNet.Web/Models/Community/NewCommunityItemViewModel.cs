namespace _1xNet.Web.Models.Community;

public class NewCommunityItemViewModel
{
    public string Title { get; set; }
    public string Description { get; set; }
    public string Tag { get; set; }
    public string ImageUrl { get; set; }
    public string ReddyLink { get; set; }
    public string Category { get; set; }
}