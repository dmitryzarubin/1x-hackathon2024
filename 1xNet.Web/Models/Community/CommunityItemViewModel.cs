using _1xNet.Web.Models.Event;

namespace _1xNet.Web.Models.Community;

public class CommunityItemViewModel
{
    public Guid Id { get; set; }
    public string Title { get; set; }
    public string Description { get; set; }
    public TagViewModel[] Tags { get; set; }
    public string ImageUrl { get; set; }
    public string ReddyLink { get; set; }
    public int MembersCount { get; set; }
    public EventsViewModel Events { get; set; }
    public IdNameViewModel Category { get; set; }
    public bool IsJoinedNow { get; set; }
}