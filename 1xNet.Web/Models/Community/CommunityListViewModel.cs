namespace _1xNet.Web.Models.Community;

public class CommunityListViewModel
{
    public IList<CommunityItemViewModel> Items { get; set; }
    public TagViewModel[] Tags { get; set; }
}